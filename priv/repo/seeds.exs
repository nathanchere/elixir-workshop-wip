AbsenceRegister.Repo.insert!(%AbsenceRegister.User{
  id: 1,
  email: "testemail@fictionaldomain.com",
  name: "Testy McTestface"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.User{
  id: 2,
  email: "nathanchere@gmail.com",
  name: "Testy McTestface"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.User{
  id: 3,
  email: "nathan.chere@tretton37.com",
  name: "Nathan Chere"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.AbsenceRequests.AbsenceRequest{
  start_date: ~D[2018-01-01],
  end_date: ~D[2018-01-03],
  user_id: 1,
  request_reason: "Recovering from New Years resolutions"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.AbsenceRequests.AbsenceRequest{
  start_date: ~D[2018-12-24],
  end_date: ~D[2018-12-28],
  user_id: 1,
  request_reason: "Celebrating Santa's birthday"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.AbsenceRequests.AbsenceRequest{
  start_date: ~D[2018-06-06],
  end_date: ~D[2018-06-06],
  user_id: 2,
  request_reason: "Listening to Sabaton"
})

AbsenceRegister.Repo.insert!(%AbsenceRegister.AbsenceRequests.AbsenceRequest{
  start_date: ~D[2018-08-11],
  end_date: ~D[2018-11-23],
  user_id: 2,
  request_reason: "Avoiding Sharepoint assignment"
})
