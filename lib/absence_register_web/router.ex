defmodule AbsenceRegisterWeb.Router do
  use AbsenceRegisterWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(AbsenceRegisterWeb.Plugs.SetUser)
    plug(AbsenceRegisterWeb.Plugs.SetAdmin)
  end

  pipeline :require_auth do
    plug(:browser)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", AbsenceRegisterWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
  end

  scope "/", AbsenceRegisterWeb do
    pipe_through(:require_auth)

    resources("/absences", AbsenceRequestController)
  end

  scope "/auth", AbsenceRegisterWeb do
    pipe_through(:browser)

    get("/google", AuthController, :request)
    get("/google/callback", AuthController, :callback)
    get("/signout", AuthController, :signout)
  end
end
