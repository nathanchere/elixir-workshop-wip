defmodule AbsenceRegisterWeb.Plugs.SetAdmin do
  import Plug.Conn
  alias AbsenceRegister.User

  def init(_params) do
  end

  def call(conn, _params) do
    domain =
      conn.assigns[:user]
      |> get_domain

    conn
    |> assign(:is_admin, is_admin?(domain))
  end

  defp get_domain(%User{} = user) do
    user.email
    |> String.split("@")
    |> Enum.fetch!(1)
  end

  defp get_domain(_), do: nil

  defp is_admin?("tretton37.com"), do: true
  defp is_admin?(_), do: false
end
