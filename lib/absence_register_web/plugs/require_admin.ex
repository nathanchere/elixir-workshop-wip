defmodule AbsenceRegisterWeb.Plugs.RequireAdmin do
  import Plug.Conn
  alias AbsenceRegister.User

  def init(_params) do
  end

  def call(conn, _params) do
    if conn.assigns[:is_admin] do
      conn
    else
      conn
      |> put_status(403)
      |> halt()
    end
  end
end
