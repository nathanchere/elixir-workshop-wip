defmodule AbsenceRegisterWeb.PageController do
  use AbsenceRegisterWeb, :controller

  def index(conn, _params) do
    conn
    |> render("index.html")
  end
end
