defmodule AbsenceRegisterWeb.AbsenceRequestController do
  use AbsenceRegisterWeb, :controller

  alias AbsenceRegister.AbsenceRequests
  alias AbsenceRegister.AbsenceRequests.AbsenceRequest

  def index(conn, _params) do
    %{id: user_id} = conn.assigns[:user]
    is_admin = conn.assigns[:is_admin]
    absence_request = AbsenceRequests.list_absence_request(user_id, is_admin)
    render(conn, "index.html", absence_request: absence_request)
  end

  def new(conn, _params) do
    changeset = AbsenceRequests.change_absence_request(%AbsenceRequest{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"absence_request" => absence_request_params}) do
    case AbsenceRequests.create_absence_request(absence_request_params) do
      {:ok, absence_request} ->
        conn
        |> put_flash(:info, "Absence request created successfully.")
        |> redirect(to: absence_request_path(conn, :show, absence_request))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    absence_request = AbsenceRequests.get_absence_request!(id)
    render(conn, "show.html", absence_request: absence_request)
  end

  def edit(conn, %{"id" => id}) do
    absence_request = AbsenceRequests.get_absence_request!(id)
    changeset = AbsenceRequests.change_absence_request(absence_request)
    render(conn, "edit.html", absence_request: absence_request, changeset: changeset)
  end

  def update(conn, %{"id" => id, "absence_request" => absence_request_params}) do
    absence_request = AbsenceRequests.get_absence_request!(id)

    case AbsenceRequests.update_absence_request(absence_request, absence_request_params) do
      {:ok, absence_request} ->
        conn
        |> put_flash(:info, "Absence request updated successfully.")
        |> redirect(to: absence_request_path(conn, :show, absence_request))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", absence_request: absence_request, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    absence_request = AbsenceRequests.get_absence_request!(id)
    {:ok, _absence_request} = AbsenceRequests.delete_absence_request(absence_request)

    conn
    |> put_flash(:info, "Absence request deleted successfully.")
    |> redirect(to: absence_request_path(conn, :index))
  end
end
