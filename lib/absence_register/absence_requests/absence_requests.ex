defmodule AbsenceRegister.AbsenceRequests do
  @moduledoc """
  The AbsenceRequests context.
  """

  import Ecto.Query, warn: false
  alias AbsenceRegister.Repo

  alias AbsenceRegister.AbsenceRequests.AbsenceRequest

  @doc """
  Returns the list of absence_request.

  ## Examples

      iex> list_absence_request()
      [%AbsenceRequest{}, ...]

  """
  def list_absence_request do
    Repo.all(AbsenceRequest)
  end

  def list_absence_request(_, true), do: list_absence_request()

  def list_absence_request(user_id, _) do
    Repo.all(
      from(r in AbsenceRegister.AbsenceRequests.AbsenceRequest,
        where: r.user_id == ^user_id
      )
    )
  end

  @doc """
  Gets a single absence_request.

  Raises `Ecto.NoResultsError` if the Absence request does not exist.

  ## Examples

      iex> get_absence_request!(123)
      %AbsenceRequest{}

      iex> get_absence_request!(456)
      ** (Ecto.NoResultsError)

  """
  def get_absence_request!(id), do: Repo.get!(AbsenceRequest, id)

  @doc """
  Creates a absence_request.

  ## Examples

      iex> create_absence_request(%{field: value})
      {:ok, %AbsenceRequest{}}

      iex> create_absence_request(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_absence_request(attrs \\ %{}) do
    %AbsenceRequest{}
    |> AbsenceRequest.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a absence_request.

  ## Examples

      iex> update_absence_request(absence_request, %{field: new_value})
      {:ok, %AbsenceRequest{}}

      iex> update_absence_request(absence_request, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_absence_request(%AbsenceRequest{} = absence_request, attrs) do
    absence_request
    |> AbsenceRequest.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a AbsenceRequest.

  ## Examples

      iex> delete_absence_request(absence_request)
      {:ok, %AbsenceRequest{}}

      iex> delete_absence_request(absence_request)
      {:error, %Ecto.Changeset{}}

  """
  def delete_absence_request(%AbsenceRequest{} = absence_request) do
    Repo.delete(absence_request)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking absence_request changes.

  ## Examples

      iex> change_absence_request(absence_request)
      %Ecto.Changeset{source: %AbsenceRequest{}}

  """
  def change_absence_request(%AbsenceRequest{} = absence_request) do
    AbsenceRequest.changeset(absence_request, %{})
  end
end
