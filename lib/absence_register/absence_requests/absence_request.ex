defmodule AbsenceRegister.AbsenceRequests.AbsenceRequest do
  use Ecto.Schema
  import Ecto.Changeset


  schema "absence_request" do
    field :end_date, :date
    field :request_reason, :string
    field :start_date, :date
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(absence_request, attrs) do
    absence_request
    |> cast(attrs, [:start_date, :end_date, :request_reason])
    |> validate_required([:start_date, :end_date, :request_reason])
  end
end
