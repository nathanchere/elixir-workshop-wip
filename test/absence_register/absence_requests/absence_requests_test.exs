defmodule AbsenceRegister.AbsenceRequestsTest do
  use AbsenceRegister.DataCase

  alias AbsenceRegister.AbsenceRequests

  describe "absence_request" do
    alias AbsenceRegister.AbsenceRequests.AbsenceRequest

    @valid_attrs %{end_date: ~D[2010-04-17], request_reason: "some request_reason", start_date: ~D[2010-04-17]}
    @update_attrs %{end_date: ~D[2011-05-18], request_reason: "some updated request_reason", start_date: ~D[2011-05-18]}
    @invalid_attrs %{end_date: nil, request_reason: nil, start_date: nil}

    def absence_request_fixture(attrs \\ %{}) do
      {:ok, absence_request} =
        attrs
        |> Enum.into(@valid_attrs)
        |> AbsenceRequests.create_absence_request()

      absence_request
    end

    test "list_absence_request/0 returns all absence_request" do
      absence_request = absence_request_fixture()
      assert AbsenceRequests.list_absence_request() == [absence_request]
    end

    test "get_absence_request!/1 returns the absence_request with given id" do
      absence_request = absence_request_fixture()
      assert AbsenceRequests.get_absence_request!(absence_request.id) == absence_request
    end

    test "create_absence_request/1 with valid data creates a absence_request" do
      assert {:ok, %AbsenceRequest{} = absence_request} = AbsenceRequests.create_absence_request(@valid_attrs)
      assert absence_request.end_date == ~D[2010-04-17]
      assert absence_request.request_reason == "some request_reason"
      assert absence_request.start_date == ~D[2010-04-17]
    end

    test "create_absence_request/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = AbsenceRequests.create_absence_request(@invalid_attrs)
    end

    test "update_absence_request/2 with valid data updates the absence_request" do
      absence_request = absence_request_fixture()
      assert {:ok, absence_request} = AbsenceRequests.update_absence_request(absence_request, @update_attrs)
      assert %AbsenceRequest{} = absence_request
      assert absence_request.end_date == ~D[2011-05-18]
      assert absence_request.request_reason == "some updated request_reason"
      assert absence_request.start_date == ~D[2011-05-18]
    end

    test "update_absence_request/2 with invalid data returns error changeset" do
      absence_request = absence_request_fixture()
      assert {:error, %Ecto.Changeset{}} = AbsenceRequests.update_absence_request(absence_request, @invalid_attrs)
      assert absence_request == AbsenceRequests.get_absence_request!(absence_request.id)
    end

    test "delete_absence_request/1 deletes the absence_request" do
      absence_request = absence_request_fixture()
      assert {:ok, %AbsenceRequest{}} = AbsenceRequests.delete_absence_request(absence_request)
      assert_raise Ecto.NoResultsError, fn -> AbsenceRequests.get_absence_request!(absence_request.id) end
    end

    test "change_absence_request/1 returns a absence_request changeset" do
      absence_request = absence_request_fixture()
      assert %Ecto.Changeset{} = AbsenceRequests.change_absence_request(absence_request)
    end
  end
end
