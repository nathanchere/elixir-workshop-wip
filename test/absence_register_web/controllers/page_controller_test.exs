defmodule AbsenceRegisterWeb.PageControllerTest do
  use AbsenceRegisterWeb.ConnCase

  test "GET / as guest", %{conn: conn} do
    validDomain = Application.get_env(:absence_register, :valid_domain)
    expected = "Sign in with your #{validDomain} account to get started"

    conn = get(conn, "/")
    assert html_response(conn, 200) =~ expected
  end

  test "GET / as authenticated user", %{conn: conn} do
    not_expected = "Sign in with your"

    conn =
      conn
      |> assign(:user, %AbsenceRegister.User{})
      |> get("/")

    assert not (html_response(conn, 200) =~ not_expected)
  end
end
